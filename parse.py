import requests
from datetime import datetime, timedelta


def parse(city, area, tip):
    row_time1 = datetime.now() - timedelta(minutes=46)
    row_time2 = datetime.now() - timedelta(minutes=31)
    time1 = row_time1.strftime("%Y-%m-%d %H:%M:%S")
    time2 = row_time2.strftime("%Y-%m-%d %H:%M:%S")
    params = {'user': 'dondigidondotcom@gmail.com',
                'token': 'aa0b555861f398e8d6b2e389f9770450',
                'category_id': '7',
                'nedvigimost_type': tip,
                'city': city,
                'date1': time1,
                'date2': time2,
                'source': '1,3,4'}
    response = requests.get("https://ads-api.ru/main/api", params=params)
    items = response.json()

    res = []
    if items['code'] == 200:
        for item in items['data']:
            if int(float(item['params'].get('Площадь'))) >= area and item['params'].get('Вид объекта') != 'Офисное помещение':
                res.append(item['url'])
    return res
    