from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from parse import parse
from database import edit_db, show_db
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import asyncio


BOT_TOKEN = "1959618510:AAFfc073LtTJwA8gzC0LO_aK6OuYYgu7tME"
bot = Bot(token=BOT_TOKEN)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class Filter(StatesGroup):
    city = State()
    area = State()


@dp.message_handler(commands=['start'])  
async def on_start(message: types.Message):
    city, area, tip = await show_db()
    res = parse(city, area, tip)
    if len(res) > 0:
        ans = ''
        for i in res:
            ans += '%s\n' % i
        await bot.send_message(message.from_user.id, ans, disable_web_page_preview=True)
    else:
        await bot.send_message(message.from_user.id, 'Нет новых объявлений за последние 15 минут')


@dp.message_handler(commands=['show'])  
async def show_ads(message: types.Message):
    while True:
        city, area, tip = await show_db()
        res = parse(city, area, tip)
        if len(res) > 0:
            ans = ''
            for i in res:
                ans += '%s\n' % i
            await bot.send_message(message.from_user.id, ans, disable_web_page_preview=True)
        else:
            await bot.send_message(message.from_user.id, 'Нет новых объявлений за последние 15 минут')
        await asyncio.sleep(900)


@dp.message_handler(commands=['filter'])  
async def edit_filters(message: types.Message):
    bt0 = InlineKeyboardButton('Регион', callback_data='city')
    bt1 = InlineKeyboardButton('Площадь', callback_data='area')
    bt2 = InlineKeyboardButton('Продажа', callback_data='tip1')
    bt3 = InlineKeyboardButton('Аренда', callback_data='tip2')
    kb = InlineKeyboardMarkup()
    kb.row(bt0, bt1)
    kb.row(bt2, bt3)
    city, area, tip = await show_db()
    ans = 'Регион: %s\nМинимальная площадь: %s' % (city, area)
    await message.reply(ans, reply=False, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'city')
async def call_city(call: types.CallbackQuery):
    await Filter.city.set()
    ans = 'Введите название региона'
    await call.message.reply(ans, reply=False)


@dp.callback_query_handler(lambda c: c.data == 'area')
async def call_area(call: types.CallbackQuery):
    await Filter.area.set()
    ans = 'Введите минимальную площадь'
    await call.message.reply(ans, reply=False)


@dp.callback_query_handler(lambda c: c.data == 'tip1')
async def call_tip1(call: types.CallbackQuery):
    await edit_db('tip', '1')
    await call.message.reply('Тип изменен на [Продажа]', reply=False)


@dp.callback_query_handler(lambda c: c.data == 'tip2')
async def call_tip2(call: types.CallbackQuery):
    await edit_db('tip', '2')
    await call.message.reply('Тип изменен на [Аренда]', reply=False)


@dp.message_handler(state=Filter.city)  
async def edit_city(message: types.Message, state: FSMContext):
    await state.finish()
    text = message.text
    ans = await edit_db('city', text)
    await message.reply(ans, reply=False)


@dp.message_handler(state=Filter.area)  
async def edit_area(message: types.Message, state: FSMContext):
    text = message.text
    if text.isdigit():
        ans = await edit_db('area', text)
        await message.reply(ans, reply=False)
        await state.finish()
    else:
        await message.reply('Введите число!', reply=False)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)