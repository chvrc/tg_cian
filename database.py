import sqlite3

def create_db():
    conn = sqlite3.connect('filters.db')
    cur = conn.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS filters(
                    id INTEGER PRIMARY KEY,
                    city TEXT,
                    area INTEGER,
                    tip INTEGER,
                    UNIQUE(id));
                    """)
    conn.commit()


async def show_db():
    conn = sqlite3.connect('filters.db')
    cur = conn.cursor()
    cur.execute("""SELECT * from filters;
                    """)
    res = cur.fetchall()
    conn.commit()
    return str(res[0][1]), int(res[0][2]), int(res[0][3])


def add_db(a, b, c):
    conn = sqlite3.connect('filters.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO filters(city, area, tip) VALUES('%s', '%s', '%s');
                """ % (a, b, c))
    res = cur.fetchall()
    conn.commit()


async def edit_db(a, b):
    conn = sqlite3.connect('filters.db')
    cur = conn.cursor()
    cur.execute("""UPDATE filters
                    SET '%s'='%s'
                    WHERE id = '1';
                    """ % (a, b))
    cur.execute("""SELECT * from filters;
                    """)
    res = cur.fetchall()
    conn.commit()
    ans = 'Регион: %s\nМинимальная площадь: %s' % (res[0][1], res[0][2])
    return ans